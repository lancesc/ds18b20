from distutils.core import setup

setup(
    name='ds18b20',
    version='0.1.0',
    author='DeLance Schmidt',
    author_email='lance.schmidt@gmail.com',
    packages=['temperature'],
    license='MIT',
    long_description=open('README.md').read(),
)
