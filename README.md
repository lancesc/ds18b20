Python Module for DS18B20 Sensor
============
  Library for accessing DS18B20 Temperature Sensor using Python on a Raspberry
  Pi.  The DS18B20 Sensor uses the Dallas Semiconductor 1-Wire interface.  A 
  kernel module is required to use the 1-wire interface on the Raspberry Pi. 
  The Occidentalis distribution for Raspberry Pi includes this module by 
  default.  If you are not using Ocidentalis, you will need to get the Dallas
  Semiconductor 1-wire interface working on your Pi.  

  1-wire is currently fixed to GPIO4, so the sensor must be attached to it.

  Written by DeLance Schmidt.

  MIT license, all text above must be included in any redistribution.

