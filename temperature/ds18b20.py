# Copyright (C) 2013 DeLance Schmidt
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


"""Module to use a Dallas Semiconductor DS18B20 on raspberry pi.

"""
import sys
import subprocess
import os
import fnmatch
import argparse
import re

W1_GPIO_MODULE = 'w1-gpio'
W1_THERM_MODULE = 'w1-therm'
W1_THERM_FAMILY_CODE = '28-'
W1_DEVICES_DIR = '/sys/bus/w1/devices'

class Error(Exception):
    pass

class DS18B20( object ):
	"""Main class"""
	def __init__(self, serial_code = None):
		"""Instantiates a DS18B20 instance"""
		self._load_1wire()
		if serial_code is None:
			self._serial_code = self._find_serial_code()
		else:
			self._serial_code = serial_code
		
		if self._serial_code is None:
			raise Error( 'Could not find a serial number for a DS18B20.' )
			
		self._filename = os.path.join(W1_DEVICES_DIR, self._serial_code, 'w1_slave' )
	
		self._crc_reg_exp = re.compile( '[0-9a-f]{2}\s[0-9a-f]{2}\s[0-9a-f]{2}\s[0-9a-f]{2}\s[0-9a-f]{2}\s[0-9a-f]{2}\s[0-9a-f]{2}\s[0-9a-f]{2}\s[0-9a-f]{2}\s:\scrc=[0-9a-f]{2}\s(\w+)' )
		self._temp_reg_exp = re.compile( '[0-9a-f]{2}\s[0-9a-f]{2}\s[0-9a-f]{2}\s[0-9a-f]{2}\s[0-9a-f]{2}\s[0-9a-f]{2}\s[0-9a-f]{2}\s[0-9a-f]{2}\s[0-9a-f]{2}\st=(\d+)' )
		
	def _load_1wire(self):
		"""Loads the 1-Wire modules"""
		self._load_kernel_module( W1_GPIO_MODULE )
		self._load_kernel_module( W1_THERM_MODULE )
	
	def _load_kernel_module(self, module):
		"""Loads a kernel module using modprobe"""
		subprocess.check_call( ['modprobe', module] )
		
	def _find_serial_code(self):
		"""Finds the first 1-wire DS18B20 based on the family code"""
		for file in os.listdir( W1_DEVICES_DIR ):
			if fnmatch.fnmatch(file, W1_THERM_FAMILY_CODE + '*'):
				return file
				
		return None
		
	def _read_w1_slave(self):
		"""Reads the file system object that contains the 1-wire temperature"""
		while True:
			with open(self._filename) as f:
				lines = f.read().split('\n')
				match = self._crc_reg_exp.match( lines[0] )
				
				if match.group(1) == 'YES':
					match = self._temp_reg_exp.match( lines[1] )
					return float( match.group(1) )
			
	
	def celsius(self):
		"""Reads the temperature in Celsius"""
		return self._read_w1_slave() / 1000
		
	def fahrenheit(self):
		"""Reads the temperature in Fahrenheit"""
		return ( 9.0 / 5.0 ) * self.celsius() + 32


def main():
	parser = argparse.ArgumentParser(description='Reads temperature from a DS18B20 1-wire sensor.')
	parser.add_argument( '--serial_code', default=None, help='Specific DB18B20 sesnor to read.' )
	parser.add_argument( '-c', '--celsius', action='store_true', help='Print temperature in Celsius.' )
	args = parser.parse_args()

	temp = DS18B20(args.serial_code)
	
	while True:
		if args.celsius:
			print '{0} C'.format( temp.celsius() )
		else:
			print '{0} F'.format( temp.fahrenheit() )

if __name__ == '__main__':
	try:
		main()
 	except KeyboardInterrupt:
		pass